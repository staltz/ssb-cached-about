# ssb-cached-about

**A client-side cache for ssb-about**

This plugin is meant for the frontend (but also can run in the backend!) of your SSB app, and uses LRU cache to avoid looking up common `ssb-about` queries.

## Usage

Prerequisities:

- `ssb-about` is installed in your app backend (in nodejs-mobile)
- React Native if running in the client
- `react-native-ssb-client` version 5 or higher

```
npm install --save ssb-cached-about
```

Install it in your `react-native-ssb-client`:

```js
import ssbClient from 'react-native-ssb-client'
import cachedAbout from 'ssb-cached-about'

// ...

ssbClient(keys, manifest)
  .use(cachedAbout())
  .call(null, (err, ssb) => {
    // ...
    // You can now call this:
    ssb.cachedAbout.socialValue({key: 'image', dest: keys.id}, (e, val) => {
      // ...
    })
  })
```

### API

- `socialValue(opts, callback)`: same as ssb-about's `socialValue()`, but has a cache built in
- `invalidate(opts)`: invalidate the cache for the ID `opts.dest`, and (OPTIONAL) field `opts.key`, where the `opts` object is similar to that one in `socialValue(opts, cb)`


## License

MIT
