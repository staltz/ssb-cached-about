import QuickLRU = require('quick-lru');
import {FeedId} from 'ssb-typescript';

const cachedAbout = {
  name: 'cachedAbout' as const,

  version: '1.0.0',

  manifest: {
    invalidate: 'sync',
    socialValue: 'async',
  },

  permissions: {
    master: {
      allow: ['invalidate', 'socialValue'],
    },
  },

  init: (ssb: any) => {
    if (!ssb?.about?.socialValue) {
      throw new Error('Plugin ssb-cached-about requires ssb-about');
    }

    const DUNBAR = 150;
    const socialValueCache = {
      name: new QuickLRU<FeedId, string>({maxSize: DUNBAR}),
      image: new QuickLRU<FeedId, any>({maxSize: DUNBAR}),
    };

    return {
      invalidate(opts: {dest: FeedId; key?: string}) {
        const {dest, key} = opts;
        if (!key) {
          socialValueCache.name.delete(dest);
          socialValueCache.image.delete(dest);
        } else if (key === 'name') {
          socialValueCache.name.delete(dest);
        } else if (key === 'image') {
          socialValueCache.image.delete(dest);
        }
      },

      socialValue(opts: any, cb: any) {
        if (opts.key === 'name' || opts.key === 'image') {
          const cache = socialValueCache[opts.key as 'name' | 'image'];
          const author = opts.dest;
          if (cache.has(author)) {
            return cb(null, cache.get(author));
          } else {
            ssb.about.socialValue(opts, (err: any, val: string) => {
              if (!err && !!val) cache.set(author, val);
              cb(err, val);
            });
          }
        } else {
          ssb.about.socialValue(opts, cb);
        }
      },
    };
  },
};

export default () => cachedAbout;
